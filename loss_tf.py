"""
@author: Tanisha R Bhayani
loss_tf.py contains loss functions used in the networks.
"""
import tensorflow as tf
import numpy as np
from utils import *
from convolution4d import *


def get_bicubic_kernel(graph, in_channels):
    """
    Args:
        graph: tensorflow graph containing the tensorflow network, specifically generator
        in_channels: input channels
    Returns: bicubic kernel for 4d images
    """
    with graph.as_default():
        bicubic_k = np.array([[0.0001373291015625, 0.0004119873046875, -0.0013275146484375, -0.0050811767578125,
                               -0.0050811767578125, -0.0013275146484375, 0.0004119873046875, 0.0001373291015625],
                              [0.0004119873046875, 0.0012359619140625, -0.0039825439453125, -0.0152435302734375,
                               -0.0152435302734375, -0.0039825439453125, 0.0012359619140625, 0.0004119873046875],
                              [-.0013275146484375, -0.0039825439453130, 0.0128326416015625, 0.0491180419921875,
                               0.0491180419921875, 0.0128326416015625, -0.0039825439453125, -0.0013275146484375],
                              [-.0050811767578125, -0.0152435302734375, 0.0491180419921875, 0.1880035400390630,
                               0.1880035400390630, 0.0491180419921875, -0.0152435302734375, -0.0050811767578125],
                              [-.0050811767578125, -0.0152435302734375, 0.0491180419921875, 0.1880035400390630,
                               0.1880035400390630, 0.0491180419921875, -0.0152435302734375, -0.0050811767578125],
                              [-.0013275146484380, -0.0039825439453125, 0.0128326416015625, 0.0491180419921875,
                               0.0491180419921875, 0.0128326416015625, -0.0039825439453125, -0.0013275146484375],
                              [0.0004119873046875, 0.0012359619140625, -0.0039825439453125, -0.0152435302734375,
                               -0.0152435302734375, -0.0039825439453125, 0.0012359619140625, 0.0004119873046875],
                              [0.0001373291015625, 0.0004119873046875, -0.0013275146484375, -0.0050811767578125,
                               -0.0050811767578125, -0.0013275146484375, 0.0004119873046875, 0.0001373291015625]])

        print(bicubic_k.shape)

        b = []

        for i in range(5):
            temp = []
            for j in range(5):
                temp.append(bicubic_k.tolist())
            b.append(temp)

        print(np.array(b).shape)
        print(np.tile(b, in_channels**2).reshape((5,5,8,8,in_channels,in_channels)).shape)
        b = np.tile(b, in_channels**2).reshape((5,5,8,8,in_channels,in_channels))
        # b1 = np.expand_dims(np.array(b), axis=0)
        b1 = b

        bicubic_kernel = tf.constant(b1, dtype=tf.float64)
        print(bicubic_kernel.shape)

        return bicubic_kernel


def GANLoss(predictions, real=False, graph=None):
    """
    Args:
        predictions: predictions from the network
        real: whether the predictions are from real varaible or fake ones.
        graph: tensorlfow graph for the network

    Returns: generative loss.
    D outputs a [0,1] map of size of the input. This map is compared in a pixel-wise manner to 1/0 according to
        whether the input is real (i.e. from the input image) or fake (i.e. from the Generator)
    """
    with graph.as_default():
        # Determine label map according to whether current input to discriminator is real or fake
        labels = tf.cond(real, lambda : tf.ones_like(predictions), lambda : tf.zeros_like(predictions))
        # Compute the loss
        return tf.losses.absolute_difference(predictions, labels)


def GANLoss_disc(predictions, loss1, real=False, graph=None):
    """
    Args:
        predictions:predictions from the network
        loss1: the above generative loss that needs to be used for the discriminator total loss
        real: whether the predictions are from real or fake input
        graph: disciriminator graph
    Returns:
        total discirminator loss

    """
    with graph.as_default():
        # Determine label map according to whether current input to discriminator is real or fake
        labels = tf.cond(real, lambda : tf.ones_like(predictions), lambda : tf.zeros_like(predictions))
        # Compute the loss
        return 0.5*(tf.losses.absolute_difference(predictions, labels) + loss1)


def resize_tensor_w_kernel(g_input, kernel, scale_factor, graph=None):
    """
    Args:
        g_input: input tensor, predictions
        kernel: bicubic kernel
        scale_factor: downscaling factor
        graph: tensorflow graph

    Returns: concolution over the input and the kernel
    """
    # Convolves a tensor with a given bicubic kernel according to scale factor
    with graph.as_default():
        stride = [round(1/scale_factor)]*4
        stride.append(1)
        stride.insert(0, 1)
        print(stride, len(stride), g_input.dtype, kernel.dtype, g_input.shape, kernel.shape)
        return convolve4d(g_input, kernel, strides=stride, padding='SAME', graph=graph)


def shave_a2b(a, b):
    """Given a big image or tensor 'a', shave it symmetrically into b's shape"""
    # If dealing with a tensor should shave the 3rd & 4th dimension, o.w. the 1st and 2nd
    is_tensor = (type(a) == tf.Tensor)
    print(is_tensor, a.shape, b.shape)
    r = 2 if is_tensor else 0
    c = 3 if is_tensor else 1
    # Calculate the shaving of each dimension
    shave_r, shave_c = max(0, a.shape[r] - b.shape[r]), max(0, a.shape[c] - b.shape[c])
    return a[:, :, shave_r // 2:a.shape[r] - shave_r // 2 - shave_r % 2, shave_c // 2:a.shape[c] - shave_c // 2 - shave_c % 2] if is_tensor \
        else a[shave_r // 2:a.shape[r] - shave_r // 2 - shave_r % 2, shave_c // 2:a.shape[c] - shave_c // 2 - shave_c % 2]


def DownsaleLoss(g_input, g_output, scale_factor, bicubic_kernel, graph=None):
    """
    Args:
        g_input: generator input
        g_output: generator output
        scale_factor: downscaling factor of the image
        bicubic_kernel: the bicubic kernel for 4d image
        graph: tensorflow graph

    Returns: downscale loss
    """
    # Computes the difference between the Generator's downscaling and an ideal (bicubic) downscaling
    with graph.as_default():
        downscaled = resize_tensor_w_kernel(g_input, tf.cast(bicubic_kernel, g_input.dtype), scale_factor, graph)
        return tf.losses.mean_squared_error(g_output, shave_a2b(downscaled, g_output))


def SumOfWeightsLoss(kernel, graph=None):
    """
    Args:
        kernel: the kernels from the generator
        graph: tensorflow graph of the generator

    Returns: the absolute difference loss tensor
    """
    # Encourages the kernel G is imitating to sum to 1
    with graph.as_default():
        sum_kernel = tf.reduce_sum(kernel)
        return tf.losses.absolute_difference(tf.ones_like(sum_kernel), sum_kernel)


def CentralizedLoss(kernel, scale_factor=0.5):
    """
    Args:
        kernel: the generator kernels
        scale_factor: the downsampled scale factor

    Returns:Return the loss over the distance of center of mass from kernel center

    """
    # following are the steps done to Penalizes distance of center of mass from Kernel's center
    print('kenrel = ', kernel.dtype, kernel.shape)
    kernel_size1 = kernel.shape.as_list()[1]
    kernel_size2 = kernel.shape.as_list()[3]
    indices1 = tf.reshape(
        tf.range(
            (kernel_size1**2)*kernel.shape.as_list()[0],
            dtype=kernel.dtype
        ),
        (kernel.shape[0], kernel_size1, kernel_size1, 1)
    )

    indices2 = tf.reshape(
        tf.range(
            (kernel_size2**2)*kernel.shape.as_list()[0],
            dtype=kernel.dtype
        ),
        (kernel.shape[0], kernel_size2, kernel_size2, 1)
    )

    cm1 = kernel_size1 // 2 + 0.5 * (int(1/scale_factor) - kernel_size1 % 2)
    cm2 = kernel_size2 // 2 + 0.5 * (int(1/scale_factor) - kernel_size2 % 2)

    # angular and spatial center of masses are cm1 nad cm2

    temp1 = tf.tile(
        tf.reshape(
            tf.convert_to_tensor(
                cm1
            ),
            (1,1)
        ),
        (kernel_size2, kernel_size2)
    )
    print(temp1.shape)

    temp2 = tf.tile(
        tf.reshape(
            tf.convert_to_tensor(
                cm2
            ),
            (1,1)
        ),
        (kernel_size1, kernel_size1)
    )

    print(temp2.shape)

    cm = tf.expand_dims(
        tf.expand_dims(
            tf.tensordot(temp2, temp1, axes=0),
            axis=0
        ),
        axis=-1
    )

    # combined center of mass - 4d

    if kernel.shape.as_list()[0] != 1:
        # if multiple dimension are present in the axis=0, then we need to do some house-keeping for the indices to get the final mul1, mul2
        r_sum = tf.reduce_sum(kernel, axis=[3,4,-1])
        c_sum = tf.reduce_sum(kernel, axis=[1,2,-1])
        mul1 = tf.expand_dims(tf.matmul(r_sum, indices1[:,:,:,0]) / (tf.reduce_sum(kernel) + 1e-5), axis=-1)
        mul2 = tf.expand_dims(tf.matmul(c_sum, indices2[:,:,:,0]) / (tf.reduce_sum(kernel) + 1e-5), axis=-1)
    else:
        # else we are good to go
        r_sum = tf.reduce_sum(kernel, axis=[3, 4, -1])
        c_sum = tf.reduce_sum(kernel, axis=[1, 2, -1])
        print(r_sum.shape, indices1.shape)
        mul1 = tf.matmul(r_sum, indices1) / (tf.reduce_sum(kernel) + 1e-5)
        mul2 = tf.matmul(c_sum, indices2) / (tf.reduce_sum(kernel) + 1e-5)
    # we do multiplication for obtaining weighted center of mass, that finally allows to calculate the penalty for the actual center of mass.

    final = tf.tensordot(mul1[0, :, :, 0], mul2[0, :, :, 0], axes=0)
    final = tf.expand_dims(final, axis=-1)
    final = tf.expand_dims(final, axis=0)

    # mul1 = tf.multiply(cm, indices1)

    # loss compute
    return tf.losses.mean_squared_error(cm, final)


def map2tensor(gray_map):
    return tf.convert_to_tensor(gray_map)


def create_gaussian(size, sigma1, sigma2=-1, is_tensor=False):
    """
    Args:
        size: size of the array for which gaussian kernel is created
        sigma1: standard deviation1
        sigma2: standard devaition 2
        is_tensor: whether the array needs to be tensor or not.

    Returns: gaussian kernel with above values

    """
    func1 = [np.exp(-z ** 2 / (2 * sigma1 ** 2)) / np.sqrt(2 * np.pi * sigma1 ** 2) for z in
             range(-size // 2 + 1, size // 2 + 1)]
    # angualr gaussian kernel
    func2 = func1 if sigma2 == -1 else [np.exp(-z ** 2 / (2 * sigma2 ** 2)) / np.sqrt(2 * np.pi * sigma2 ** 2) for z in
                                        range(-size // 2 + 1, size // 2 + 1)]
    # spatial gaussian kernel
    # combines the above both kernels
    return tf.convert_to_tensor(np.outer(func1, func2)) if is_tensor else np.outer(func1, func2)


def create_penalty_mask(kernel_size1, kernel_size2, penalty_scale):
    """
    Args:
        kernel_size1: angualr kernel size
        kernel_size2: spatial kernel size
        penalty_scale: penalty to assign for boundary loss

    Returns: Generate a mask of weights penalizing values close to the boundaries

    """
    # calculate the center of the kernel
    center_size1 = kernel_size1 // 2 + kernel_size1 % 2
    center_size2 = kernel_size2 // 2 + kernel_size2 % 2

    # create gaussian mask
    mask1 = create_gaussian(size=kernel_size1, sigma1=kernel_size1)
    mask2 = create_gaussian(size=kernel_size2, sigma1=kernel_size2)

    # normalize it
    mask1 = 1 - mask1 / np.max(mask1)
    mask2 = 1 - mask2 / np.max(mask2)

    # get hthe border margins
    margin1 = (kernel_size1 - center_size1) // 2 - 1
    margin2 = (kernel_size2 - center_size2) // 2 - 1

    # set them to 0 for penalizing them
    mask1[margin1:-margin1, margin1:-margin1] = 0
    mask2[margin2:-margin2, margin2:-margin2] = 0

    final = tf.expand_dims(tf.expand_dims(tf.tensordot(mask1, mask2, axes=0), axis=0), axis=-1)

    # return penalty scaled mask
    return final * penalty_scale


def BoundariesLoss(kernel):
    """
    Args:
        kernel: generator kernels

    Returns: loss tensor between penalty mask * kernel and zeros

    """
    # Encourages sparsity of the boundaries by penalizing non-zeros far from the center
    kernel_size1 = kernel.shape.as_list()[1]
    kernel_size2 = kernel.shape.as_list()[3]
    mask = tf.cast(create_penalty_mask(kernel_size1, kernel_size2, 30), dtype=tf.float32)
    zero_label = tf.zeros_like(kernel, dtype=tf.float32)
    # multiply kernel and penalty mask to get non-zero values
    temp = tf.multiply(mask, kernel)

    return tf.losses.absolute_difference(temp, zero_label)


def SparsityLoss(kernel):
    """
    Args:
        kernel: generator kernel

    Returns: loss tensor between the contrasted values of kernel and the zeros values

    """
    # Penalizes small values to encourage sparsity
    power = 0.2
    return tf.losses.absolute_difference(tf.math.abs(kernel) ** power, tf.zeros_like(kernel))


def calc_curr_k(generator_model, graph=None):
    """
    Args:
        generator_model: the generator model to get each of its kernels
        graph: the tensorflow graph for this model

    Returns: the final kernel from the generator

    """
    # convolove the each layer of the model with ones value to get the actual weights fo the mdoel
    with graph.as_default():
        index = 0
        for w in tf.trainable_variables():
            if 'generator' in w.name:
                print(w.name, w.shape)
                if index == 0:
                    # if its first layer use ones variable as input
                    curr_k = tf.ones_like(generator_model.input)[:1, ...]
                # else, use the ouptut from the previosu layer as the input to the convolution to the upcoming weights
                curr_k = convolve4d(curr_k, w, padding='SAME', graph=graph)
                print('curr_k = ', curr_k.shape)
                index += 1
        # given a generator network, the function calculates the kernel it is imitating
        return curr_k


# if __name__ == '__main__':
#     a = tf.zeros(shape=(1,5,5,64,64,1), dtype=tf.float64)
#     b = tf.zeros(shape=(1,5,5,64,64,1))
#     value = DownsaleLoss(a, b, 1)
#
#     from networks_tf import *
#     from easydict import EasyDict
#
#     batch = 1
#     x_dim = 5
#     y_dim = 5
#     z_dim = 64
#     t_dim = 64
#     in_channels = 1
#     out_channels = 3
#
#     a = tf.zeros(
#         shape=[batch, x_dim, y_dim, z_dim, t_dim, in_channels]
#     )
#
#     conf = EasyDict()
#     conf.G_chan = 1
#     conf.D_chan = 1
#     conf.D_kernel_size = 5
#     conf.D_n_layers = 3
#     conf.G_structure = [5, 5, 5]
#     conf.scale_factor = 1
#     #
#     gen = Generator()
#     gen_model = gen.build_generator(conf, a)
#
#     # model = Generator()
#     # g = model.build_generator(conf, a)
#
#     kernel = calc_curr_k(gen)
#
#     print('kernel.shape = ', kernel.shape)
#
#     value = CentralizedLoss(5, 64, kernel, scale_factor=0.5)
#
#     print(value)
#
#     value1 = create_penalty_mask(5, 64, penalty_scale=30)
#     print(value1.shape)
#
#     value2 = BoundariesLoss(5, 64, kernel)

