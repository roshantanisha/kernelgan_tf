import tensorflow as tf
import numpy as np
from log import logging
from tqdm import tqdm


def initializer_conv4d(in_channels, out_channels, mapsize, mapsize2,
                       stddev_factor=1.0, mode='Glorot', graph=None):
    """
    args:
        in_channels: channels/filters from the incoming layer
        out_channels: channels that are result of the convolution operation, or total number of filters in the current convolution operation
        mapsize: angular kernel size
        mapsize2: spatial kernel size
        stddev_factor: standard deviation for initializing the kernel
        mode: type of initialization
        graph: tensorflow graph for the current model
    remarks:
        Initialization in the style of Glorot 2010.
        stddev_factor should be 1.0 for linear activations, and 2.0 for ReLUs"""
    with graph.as_default():
        if mode == 'Glorot':
            # calculate std for the Glorot model
            stddev = np.sqrt(
                stddev_factor / (np.sqrt(in_channels * out_channels) * mapsize * mapsize * mapsize2 * mapsize2))
        else:
            stddev = 1.0

        # create the kernel
        init_value = tf.truncated_normal([mapsize, mapsize, mapsize2, mapsize2, in_channels, out_channels],
                            mean=0.0, stddev=stddev)
        return init_value


def conv4d(x, in_channels, out_channels, kernel_size_1=3, kernel_size_2=3,
           stride_1=1, stride_2=1, padding='SAME', stddev_factor=1.0, trainable=True, verbose=True, bias=True,
           activation='relu', name='convolution', graph=None):
    """
        args:
            x: the input tensor
            in_channels: the filters from the previous layer
            out_channels: the output filters from teh current convolution operation
            kernel_size1: angular kernel size
            kernel_size2: spatial kernel size
            stride_1: angular stride
            stride_2: spacial stride
            padding: whether the padding should be same or valid , we use same padding only throughout all networks
            stddev_factor: standard deviation
            trainable: whether the variable is trainable or not
            verbose: whether to log the message or not
            bias: add bias to the variable or not
            activation: which activation to use
            name: the name of the variable to distinguish it
            graph: graph of the tensorflow network.
    """
    assert len(x.get_shape().as_list()) == 6 and \
           "Previous layer must be 6D: (batch, height, width, sview, tview, channels)"

    with graph.as_default():
        # get the kerenl weights
        weight4d_init = initializer_conv4d(in_channels, out_channels, mapsize=kernel_size_1,
                                       mapsize2=kernel_size_2, stddev_factor=stddev_factor, graph=graph)

    with graph.as_default():
        # create a variable for the filter and initialize it with the kernel weights
        filter4d = tf.get_variable(name='weight_' + name,
                                   initializer=weight4d_init,
                                   dtype=x.dtype,
                                   trainable=trainable)

    with graph.as_default():
        # create the convolution tensor that does the convolitoin 4d on the input tensor and hte kernel
        out = convolve4d(input=x, filter=filter4d,
                         strides=[1, stride_1, stride_1, stride_2, stride_2, 1],
                         padding=padding, bias=bias, activation=activation, graph=graph)

    if verbose:
        message = '|{0:-^72}|'.format(' Conv4d Layer: ' + str(out.get_shape()) + ' ')
        logging.info(message)
    return out


def convolve4d(input, filter,
               strides=[1, 1, 1, 1, 1, 1],
               padding='SAME',
               dilation_rate=None,
               stack_axis=None,
               stack_nested=False,
               bias=False,
               activation='relu', graph=None, name='name'
               ):
    '''
      Computes a convolution over 4 dimensions.
      Python generalization of tensorflow's conv3d with dilation.
      conv4d_stacked uses tensorflows conv3d and stacks results along
      stack_axis.

  Parameters
  ----------
  input : A Tensor.
          Shape [batch, x_dim, y_dim, z_dim, t_dim, in_channels]

  filter: A Tensor. Must have the same type as input.
          Shape [x_dim, y_dim, z_dim, t_dim, in_channels, out_channels].
          in_channels must match between input and filter

  strides: A list of ints that has length 6. 1-D tensor of length 6.
           The stride of the sliding window for each dimension of input.
           Must have strides[0] = strides[5] = 1.
  padding: A string from: "SAME", "VALID". The type of padding algorithm to use.

  dilation_rate: Optional. Sequence of 4 ints >= 1.
                 Specifies the filter upsampling/input downsampling rate.
                 Equivalent to dilation_rate in tensorflows tf.nn.convolution

  stack_axis: Int
            Axis along which the convolutions will be stacked.
            By default the axis with the lowest output dimensionality will be
            chosen. This is only an educated guess of the best choice!

  stack_nested: Bool
            If set to True, this will stack in a for loop seperately and afterwards
            combine the results. In most cases slower, but maybe less memory needed.

  Returns
  -------
          A Tensor. Has the same type as input.
    '''
    stack_axis = 3

    if dilation_rate != None:
        dilation_along_stack_axis = dilation_rate[stack_axis - 1]
    else:
        dilation_along_stack_axis = 1

    with graph.as_default():
        tensors_t = tf.unstack(input, axis=stack_axis)
        kernel_t = tf.unstack(filter, axis=stack_axis - 1)

    # noOfInChannels = input.get_shape().as_list()[-1]
    len_ts = filter.get_shape().as_list()[stack_axis - 1]
    size_of_t_dim = input.get_shape().as_list()[stack_axis]

    if len_ts % 2 == 1:
        # uneven filter size: same size to left and right
        filter_l = int(len_ts / 2)
        filter_r = int(len_ts / 2)
    else:
        # even filter size: one more to right
        filter_l = int(len_ts / 2) - 1
        filter_r = int(len_ts / 2)

    # The start index is important for strides and dilation
    # The strides start with the first element
    # that works and is VALID:
    start_index = 0
    if padding == 'VALID':
        for i in tqdm(range(size_of_t_dim)):
            if len(range(max(i - dilation_along_stack_axis * filter_l, 0),
                         min(i + dilation_along_stack_axis * filter_r + 1,
                             size_of_t_dim), dilation_along_stack_axis)
                   ) == len_ts:
                # we found the first index that doesn't need padding
                break
        start_index = i
        # print 'start_index', start_index

    # loop over all t_j in t
    result_t = []
    for i in range(start_index, size_of_t_dim, strides[stack_axis]):

        kernel_patch = []
        input_patch = []
        tensors_t_convoluted = []

        if padding == 'VALID':

            # Get indices t_s
            indices_t_s = range(max(i - dilation_along_stack_axis * filter_l, 0),
                                min(i + dilation_along_stack_axis * filter_r + 1, size_of_t_dim),
                                dilation_along_stack_axis)

            # check if Padding = 'VALID'
            if len(indices_t_s) == len_ts:

                # sum over all remaining index_t_i in indices_t_s
                for j, index_t_i in enumerate(indices_t_s):
                    if not stack_nested:
                        with graph.as_default():
                            kernel_patch.append(kernel_t[j])
                            input_patch.append(tensors_t[index_t_i])
                    else:
                        if dilation_rate != None:
                            with graph.as_default():
                                temp = tf.nn.convolution(input=tensors_t[index_t_i],
                                                         filter=kernel_t[j],
                                                         strides=strides[1:stack_axis + 1] + strides[stack_axis:5],
                                                         padding=padding,
                                                         dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                    stack_axis:])
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            try:
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            except:
                                pass
                            tensors_t_convoluted.append(temp)
                        else:
                            with graph.as_default():
                                temp = tf.nn.conv3d(input=tensors_t[index_t_i],
                                                    filter=kernel_t[j],
                                                    strides=strides[:stack_axis] + strides[stack_axis + 1:],
                                                    padding=padding)
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            try:
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            except:
                                pass
                            tensors_t_convoluted.append(temp)
                if stack_nested:
                    with graph.as_default():
                        sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                    # put together
                    result_t.append(sum_tensors_t_s)

        elif padding == 'SAME':

            # Get indices t_s
            indices_t_s = range(i - dilation_along_stack_axis * filter_l,
                                (i + 1) + dilation_along_stack_axis * filter_r,
                                dilation_along_stack_axis)

            for kernel_j, j in enumerate(indices_t_s):
                # we can just leave out the invalid t coordinates
                # since they will be padded with 0's and therfore
                # don't contribute to the sum

                if 0 <= j < size_of_t_dim:
                    if not stack_nested:
                        with graph.as_default():
                            kernel_patch.append(kernel_t[kernel_j])
                            input_patch.append(tensors_t[j])
                    else:
                        if dilation_rate != None:
                            with graph.as_default():
                                temp = tf.nn.convolution(input=tensors_t[j],
                                                         filter=kernel_t[kernel_j],
                                                         strides=strides[1:stack_axis + 1] + strides[stack_axis:5],
                                                         padding=padding,
                                                         dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                        stack_axis:])
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            try:
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            except:
                                pass
                            tensors_t_convoluted.append(temp)
                        else:
                            with graph.as_default():
                                temp = tf.nn.conv3d(input=tensors_t[j],
                                                    filter=kernel_t[kernel_j],
                                                    strides=strides[:stack_axis] + strides[stack_axis + 1:],
                                                    padding=padding, bias=bias)
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            try:
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            except:
                                pass
                            tensors_t_convoluted.append(temp)
            if stack_nested:
                with graph.as_default():
                    sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                # put together
                result_t.append(sum_tensors_t_s)

        if not stack_nested:
            if kernel_patch:
                with graph.as_default():
                    kernel_patch = tf.concat(kernel_patch, axis=3)
                    input_patch = tf.concat(input_patch, axis=4)
                    kernel_patch1 = kernel_patch
                    # kernel_patch1 = tf.transpose(kernel_patch, (0, 1, 2, 4, 3))
                if dilation_rate != None:
                    with graph.as_default():
                        result_patch = tf.nn.convolution(input=input_patch,
                                                         filter=kernel_patch1,
                                                         strides=strides[1:stack_axis] + strides[stack_axis + 1:5],
                                                         padding=padding,
                                                         dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                        stack_axis:])
                    # result_patch = tf.transpose(result_patch, (0, 1, 2, 4, 3))
                    if bias:
                        with graph.as_default():
                            zeros1 = tf.zeros(shape=(result_patch.shape.as_list()[-1]))
                            result_patch = tf.nn.bias_add(result_patch, zeros1)
                    try:
                        with graph.as_default():
                            temp = getattr(tf.nn, activation)(temp)
                    except:
                        pass
                else:
                    with graph.as_default():
                        result_patch = tf.nn.conv3d(input=input_patch,
                                                    filters=kernel_patch1,
                                                    # output_shape=input_patch.shape,
                                                    strides=strides[:stack_axis] + strides[stack_axis + 1:],
                                                    padding=padding)
                        # result_patch = tf.transpose(result_patch, (0, 1, 2, 4, 3))
                    if bias:
                        with graph.as_default():
                            zeros2 = tf.zeros(shape=(result_patch.shape.as_list()[-1]))
                            result_patch = tf.nn.bias_add(result_patch, zeros2)
                    try:
                        with graph.as_default():
                            temp = getattr(tf.nn, activation)(temp)
                    except:
                        pass
                result_t.append(result_patch)

    # stack together
    with graph.as_default():
        return_value = tf.stack(result_t, axis=stack_axis, name='4dconvolution_' + name)
    return return_value


# def transpose_conv(i, k, strides):
def convolve4d_transpose(input, filter,
                         strides=[1, 1, 1, 1, 1, 1],
                         padding='SAME',
                         dilation_rate=None,
                         stack_axis=None,
                         stack_nested=False,
                         ):
    '''
      Computes a convolution over 4 dimensions.
      Python generalization of tensorflow's conv3d with dilation.
      conv4d_stacked uses tensorflows conv3d and stacks results along
      stack_axis.

  Parameters
  ----------
  input : A Tensor.
          Shape [batch, x_dim, y_dim, z_dim, t_dim, in_channels]

  filter: A Tensor. Must have the same type as input.
          Shape [x_dim, y_dim, z_dim, t_dim, out_channels, in_channels].
          in_channels must match between input and filter

  strides: A list of ints that has length 6. 1-D tensor of length 6.
           The stride of the sliding window for each dimension of input.
           Must have strides[0] = strides[5] = 1.
  padding: A string from: "SAME", "VALID". The type of padding algorithm to use.

  dilation_rate: Optional. Sequence of 4 ints >= 1.
                 Specifies the filter upsampling/input downsampling rate.
                 Equivalent to dilation_rate in tensorflows tf.nn.convolution

  stack_axis: Int
            Axis along which the convolutions will be stacked.
            By default the axis with the lowest output dimensionality will be
            chosen. This is only an educated guess of the best choice!

  stack_nested: Bool
            If set to True, this will stack in a for loop seperately and afterwards
            combine the results. In most cases slower, but maybe less memory needed.

  Returns
  -------
          A Tensor. Has the same type as input.
    '''

    stack_axis = 3
    output_shape = [input.shape[i] + filter.shape[i - 1] - strides[i - 1] for i in range(1, len(input.shape))]
    output_shape.append(filter.shape[-1])

    if dilation_rate != None:
        dilation_along_stack_axis = dilation_rate[stack_axis - 1]
    else:
        dilation_along_stack_axis = 1

    # prepare kernels and tensors for the convolution 3d operation
    tensors_t = tf.unstack(input, axis=stack_axis)
    kernel_t = tf.unstack(filter, axis=stack_axis - 1)

    noOfInChannels = input.get_shape().as_list()[-1]
    len_ts = filter.get_shape().as_list()[stack_axis - 1]
    size_of_t_dim = input.get_shape().as_list()[stack_axis]

    if len_ts % 2 == 1:
        # uneven filter size: same size to left and right
        filter_l = int(len_ts / 2)
        filter_r = int(len_ts / 2)
    else:
        # even filter size: one more to right
        filter_l = int(len_ts / 2) - 1
        filter_r = int(len_ts / 2)

    # The start index is important for strides and dilation
    # The strides start with the first element
    # that works and is VALID:
    start_index = 0
    if padding == 'VALID':
        for i in tqdm(range(size_of_t_dim)):
            if len(range(max(i - dilation_along_stack_axis * filter_l, 0),
                         min(i + dilation_along_stack_axis * filter_r + 1,
                             size_of_t_dim), dilation_along_stack_axis)
                   ) == len_ts:
                # we found the first index that doesn't need padding
                break
        start_index = i
        # print 'start_index', start_index

    # loop over all t_j in t
    result_t = []
    for i in range(start_index, size_of_t_dim, strides[stack_axis]):

        kernel_patch = []
        input_patch = []
        tensors_t_convoluted = []

        if padding == 'VALID':

            # Get indices t_s
            indices_t_s = range(max(i - dilation_along_stack_axis * filter_l, 0),
                                min(i + dilation_along_stack_axis * filter_r + 1, size_of_t_dim),
                                dilation_along_stack_axis)

            # check if Padding = 'VALID'
            if len(indices_t_s) != len_ts:

                # sum over all remaining index_t_i in indices_t_s
                for j, index_t_i in enumerate(indices_t_s):
                    if not stack_nested:
                        kernel_patch.append(kernel_t[j])
                        input_patch.append(tensors_t[index_t_i])
                    else:
                        if dilation_rate != None:
                            tensors_t_convoluted.append(tf.nn.conv_transpose(input=tensors_t[index_t_i],
                                                                             filters=kernel_t[j],
                                                                             strides=strides[
                                                                                     1:stack_axis + 1] + strides[
                                                                                                         stack_axis:5],
                                                                             padding=padding,
                                                                             output_shape=output_shape,
                                                                             dilation_rate=dilation_rate[
                                                                                           :stack_axis - 1] + dilation_rate[
                                                                                                              stack_axis:])
                                                        )
                        else:
                            tensors_t_convoluted.append(tf.nn.conv3d_transpose(input=tensors_t[index_t_i],
                                                                               filters=kernel_t[j],
                                                                               output_shape=output_shape,
                                                                               strides=strides[:stack_axis] + strides[
                                                                                                              stack_axis + 1:],
                                                                               padding=padding)
                                                        )
                if stack_nested:
                    sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                    # put together
                    result_t.append(sum_tensors_t_s)

        elif padding == 'SAME':

            # Get indices t_s
            indices_t_s = range(i - dilation_along_stack_axis * filter_l,
                                (i + 1) + dilation_along_stack_axis * filter_r,
                                dilation_along_stack_axis)

            for kernel_j, j in enumerate(indices_t_s):
                # we can just leave out the invalid t coordinates
                # since they will be padded with 0's and therfore
                # don't contribute to the sum

                if 0 <= j < size_of_t_dim:
                    if not stack_nested:
                        kernel_patch.append(kernel_t[kernel_j])
                        input_patch.append(tensors_t[j])
                    else:
                        if dilation_rate != None:
                            tensors_t_convoluted.append(tf.nn.conv_transpose(input=tensors_t[j],
                                                                             filter=kernel_t[kernel_j],
                                                                             strides=strides[
                                                                                     1:stack_axis + 1] + strides[
                                                                                                         stack_axis:5],
                                                                             padding=padding,
                                                                             output_shape=output_shape,
                                                                             dilation_rate=dilation_rate[
                                                                                           :stack_axis - 1] + dilation_rate[
                                                                                                              stack_axis:])
                                                        )
                        else:
                            tensors_t_convoluted.append(tf.nn.conv3d_transpose(input=tensors_t[j],
                                                                               filter=kernel_t[kernel_j],
                                                                               output_shape=output_shape,
                                                                               strides=strides[
                                                                                       :stack_axis] + strides[
                                                                                                      stack_axis + 1:],
                                                                               padding=padding)
                                                        )
            if stack_nested:
                sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                # put together
                result_t.append(sum_tensors_t_s)

        if not stack_nested:
            if kernel_patch:
                kernel_patch = tf.concat(kernel_patch, axis=stack_axis + 1)
                input_patch = tf.concat(input_patch, axis=stack_axis + 1)
                if dilation_rate != None:
                    result_patch = tf.nn.conv_transpose(input=input_patch,
                                                        filter=kernel_patch,
                                                        output_shape=output_shape,
                                                        strides=strides[1:stack_axis] + strides[stack_axis + 1:5],
                                                        padding=padding,
                                                        dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                       stack_axis:])
                else:
                    print(input_patch.shape, kernel_patch.shape)
                    assert input_patch.shape[-1] == kernel_patch.shape[-1]
                    if padding == 'VALID':
                        # print(input_patch.shape, kernel_patch.shape)
                        # get the output shape, in convolution transpose getting the output shape before hand
                        output_shape = [(input_patch.shape[i + 1] - 1) * strides[i] + kernel_patch.shape[i] for i in
                                        range(3)]
                        output_shape.append(kernel_patch.shape[-2])
                        output_shape.insert(0, input_patch.shape[0])
                        # print('now = ', output_shape)
                    if padding == 'SAME':
                        # if the padding is same the output shaep will be same as the input shaep
                        output_shape = input_patch.shape.as_list()
                        # print(output_shape)
                        output_shape[-1] = kernel_patch.shape[-2]

                    result_patch = tf.nn.conv3d_transpose(value=input_patch,
                                                          filters=kernel_patch,
                                                          output_shape=output_shape,
                                                          strides=strides[:stack_axis + 1] + strides[stack_axis + 2:],
                                                          padding=padding)
                result_t.append(result_patch)

    # stack together
    return tf.stack(result_t, axis=stack_axis)
