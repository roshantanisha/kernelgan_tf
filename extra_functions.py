# KernelGAN specific functions


def nn_interpolation_4d(im, sf):
    interpolated = []
    print(im.shape)
    for i in range(im.shape[0]):
        array = []
        for j in range(im.shape[1]):
            temp = Image.fromarray(im[i, j, ...])
            array.append(np.array(temp.resize((temp.size[0]*sf, temp.size[1]*sf), Image.NEAREST), dtype=im.dtype).tolist())

        interpolated.append(array)
        del array

    return np.array(interpolated)


def pad_edges(im, edge):
    """Replace image boundaries with 0 without changing the size"""
    zero_padded = np.zeros_like(im)
    zero_padded[:, :, edge:-edge, edge:-edge, ...] = im[:, :, edge:-edge, edge:-edge]
    return zero_padded


def clip_extreme(im, percent):
    """Zeroize values below the a threshold and clip all those above"""
    # Sort the image
    im_sorted = np.sort(im.flatten())
    # Choose a pivot index that holds the min value to be clipped
    pivot = int(percent * len(im_sorted))
    v_min = im_sorted[pivot]
    # max value will be the next value in the sorted array. if it is equal to the min, a threshold will be added
    v_max = im_sorted[pivot + 1] if im_sorted[pivot + 1] > v_min else v_min + 10e-6
    # Clip an zeroize all the lower values
    return np.clip(im, v_min, v_max) - v_min


def create_gradient_map(im, window=5, percent=.97):
    """Create a gradient map of the image blurred with a rect of size window and clips extreme values"""
    # Calculate gradients
    img1 = convert_rgb_to_gray(im)
    grad = np.gradient(np.squeeze(img1))
    # Calculate gradient magnitude
    gmag, grad = np.sqrt(np.sum(np.stack(grad, axis=0) ** 2, axis=0)), np.abs(grad)
    # Pad edges to avoid artifacts in the edge of the image
    grad_pad = []
    for each_grad in grad:
        print(each_grad.shape)
        grad_pad.append(pad_edges(each_grad, int(window)))
    gmag = pad_edges(gmag, int(window))
    lm_grad = []
    for each_grad in grad:
        lm_grad.append(clip_extreme(each_grad, percent))
    lm_gmag = clip_extreme(gmag, percent)

    # Sum both gradient maps
    for index in range(len(lm_grad)):
        lm_grad[index] = lm_grad[index] / lm_grad[index].sum()

    grads_comb = np.sum(lm_grad, axis=0) + lm_gmag / lm_gmag.sum()
    # Blur the gradients and normalize to original values
    print('grads_comb.shape = ', grads_comb.shape, window)
    loss_map = convolve(grads_comb, np.ones(shape=(window, window, window, window))) / (window ** 2)
    # Normalizing: sum of map = numel
    print(loss_map.shape)
    return loss_map / np.mean(loss_map)


def create_probability_map(loss_map, crop):
    """Create a vector of probabilities corresponding to the loss map"""
    # Blur the gradients to get the sum of gradients in the crop
    print(loss_map.shape)
    blurred = convolve(loss_map, np.ones([8, 8, 8, 8]), ) / ((crop // 2) ** 2)
    # Zero pad s.t. probabilities are NNZ only in valid crop centers
    prob_map = pad_edges(blurred, crop // 2)
    # Normalize to sum to 1
    prob_vec = prob_map.flatten() / prob_map.sum() if prob_map.sum() != 0 else np.ones_like(prob_map.flatten()) / prob_map.flatten().shape[0]
    return prob_vec


class DataGenerator:
    """
    The data generator loads an image once, calculates it's gradient map on initialization and then outputs a cropped version
    of that image whenever called.
    """

    # def __init__(self, conf, image, gan):
    def __init__(self, conf, image):
        # Default shapes
        self.g_input_shape = conf.input_crop_size
        # self.d_input_shape = gan.G.output_size[2]  # shape entering D downscaled by G
        self.d_input_shape = conf.output_size
        # self.d_output_shape = self.d_input_shape - gan.D.forward_shave
        self.d_output_shape = self.d_input_shape - conf.forward_shave

        # Read input image
        # self.input_image = read_image(conf.input_image_path) / 255.
        self.input_image = image / 255.
        self.shave_edges(scale_factor=conf.scale_factor, real_image=conf.real_image)

        self.in_rows, self.in_cols = self.input_image.shape[0:2]

        # Create prob map for choosing the crop
        self.crop_indices_for_g, self.crop_indices_for_d = self.make_list_of_crop_indices(conf=conf)

    def __len__(self):
        return 1

    def __getitem__(self, idx):
        """Get a crop for both G and D """
        print(idx)
        g_in = self.next_crop(for_g=True, idx=idx)
        d_in = self.next_crop(for_g=False, idx=idx)

        return g_in, d_in

    def next_crop(self, for_g, idx):
        """Return a crop according to the pre-determined list of indices. Noise is added to crops for D"""
        size = self.g_input_shape if for_g else self.d_input_shape
        top, left = self.get_top_left(size, for_g, idx)
        print(top, left)
        crop_im = self.input_image[top:top + size, left:left + size, ...]
        print(crop_im.shape, 'crop_imjansdfjabfgkan')
        if not for_g:  # Add noise to the image for d
            crop_im += np.random.randn(*crop_im.shape) / 255.0
        # return im2tensor(crop_im)
        return crop_im

    def make_list_of_crop_indices(self, conf):
        iterations = conf.max_iters
        prob_map_big, prob_map_sml = self.create_prob_maps(scale_factor=conf.scale_factor)
        crop_indices_for_g = np.random.choice(a=len(prob_map_sml), size=iterations, p=prob_map_sml)
        crop_indices_for_d = np.random.choice(a=len(prob_map_big), size=iterations, p=prob_map_big)
        print(crop_indices_for_g, crop_indices_for_d)
        return crop_indices_for_g, crop_indices_for_d

    def create_prob_maps(self, scale_factor):
        # Create loss maps for input image and downscaled one
        loss_map_big = create_gradient_map(self.input_image)
        temp = np.squeeze(imresize(im=np.expand_dims(self.input_image, axis=0), scale_factor=scale_factor, kernel='cubic'))
        print(temp.shape)
        loss_map_sml = create_gradient_map(temp)
        # Create corresponding probability maps
        prob_map_big = create_probability_map(loss_map_big, self.d_input_shape)
        temp = nn_interpolation_4d(loss_map_sml, int(1 / scale_factor))
        prob_map_sml = create_probability_map(temp, self.g_input_shape)
        return prob_map_big, prob_map_sml

    def shave_edges(self, scale_factor, real_image):
        """Shave pixels from edges to avoid code-bugs"""
        # Crop 10 pixels to avoid boundaries effects in synthetically generated examples
        if not real_image:
            self.input_image = self.input_image[10:-10, 10:-10, :]
        # Crop pixels for the shape to be divisible by the scale factor
        sf = int(1 / scale_factor)
        shape = self.input_image.shape
        self.input_image = self.input_image[:-(shape[0] % sf), :, :] if shape[0] % sf > 0 else self.input_image
        self.input_image = self.input_image[:, :-(shape[1] % sf), :] if shape[1] % sf > 0 else self.input_image

    def get_top_left(self, size, for_g, idx):
        """Translate the center of the index of the crop to it's corresponding top-left"""
        center = self.crop_indices_for_g[idx] if for_g else self.crop_indices_for_d[idx]
        row, col = int(center / self.in_cols), center % self.in_cols
        top, left = min(max(0, row - size // 2), self.in_rows - size), min(max(0, col - size // 2), self.in_cols - size)
        # Choose even indices (to avoid misalignment with the loss map for_g)
        return top - top % 2, left - left % 2