import cv2
import math
import numpy as np
import matplotlib.pyplot as plt
import lmdb
import pandas as pd
import os
import zlib
from scipy.ndimage import convolve
from imresize import imresize
from PIL import Image


# constants to stanford dataset
H = 128
W = 128
H1 = 376
W1 = 541


def get_lf_array(raw_array, C):
    # get the light field array (S, T, H, W, C) from the raw array given the C channels
    S = 14
    T = 14
    H = 376
    W = 541
    # C = 3
    print(raw_array.shape)
    lf = np.zeros(shape=(S, T, H, W, C), dtype=np.uint8)
    for x in range(S):
        for y in range(T):
            # sample every Sth, and Tth pixel starting from x, y position to get the light field array at the (x, y) angular position
            temp = raw_array[x::S, y::T, :]
            # print(temp.shape, temp[0, 0, ...].shape)
            if len(raw_array.shape) == 3:
                lf[x, y, :temp.shape[0], :temp.shape[1], :] = temp
            else:
                lf[x, y, :temp.shape[2], :temp.shape[3], :] = temp[0, 0, ...]

    # print(lf.shape)
    return lf


def patch(image_array, patch_size, train=False, sample_patches=None, angular_patch_size=5):
    # create patches of the lf array
    channels = image_array.shape[-1]
    print('channels = ', channels)
    image_array = get_lf_array(image_array, channels)
    # get 2d shaped array
    new_image = image_array.reshape((-1, H1, W1, channels))
    # print(new_image.shape)
    H = patch_size
    W = patch_size
    # calculate the new H, and W based on the actual H1 and W1 and the patch size
    H_prime = math.ceil(H1/H)*H
    W_prime = math.ceil(W1/W)*W

    # print(H_prime, W_prime)

    new_image_prime = np.zeros(shape=(new_image.shape[0], H_prime, W_prime, channels), dtype=np.uint8)
    # print(new_image_prime.shape)
    new_image_prime[:, :H1, :W1, :] = new_image
    # create an image which is multiple in H and W of patch sizes

    # print('actual number of patches = ', new_image.shape[0]*(H_prime//H)*(W_prime//W))
    # patches = np.zeros(shape=(new_image.shape[0]*(H_prime//H)*(W_prime//W), H, W, 3), dtype=np.uint8)
    patches = []
    # print(patches.shape)
    H_multiplier = H_prime//H
    W_multiplier = W_prime//W

    i = 0
    # create patches from the lf array loop
    for each_image in new_image_prime:
        for j in range(H_multiplier):
            for k in range(W_multiplier):
                temp = each_image[j*H:H*(j+1), (k)*W:(k+1)*W]
                # if np.all(temp == 0) and train:
                #     continue
                # patches[i] = each_image[j*H:H*(j+1), (k)*W:(k+1)*W]
                patches.append(temp)
                i = i + 1

    patches = np.array(patches)
    # if train:
    print(patches.shape)
    # reshape the obtained patches to the lf array but according to the patch size, here, we have spatial patches done
    patches = patches.reshape((-1, patch_size, patch_size, 14, 14, channels))
    # if sample_patches is not None:
    #     patches = patches[np.random.choice(np.arange(patches.shape[0]), sample_patches, replace=True), ...]
    # do angular patches.
    angular_patches = patch_angular(patches, angular_patch_size=angular_patch_size)
    if sample_patches is not None:
        # if only few patches need to seleceted.
        angular_patches = angular_patches[np.random.choice(np.arange(patches.shape[0]), sample_patches, replace=True), ...]
    # return both spatial and angular patches obtained after doing spatial patches.
    return patches, angular_patches


def load_patches(db, index, patch_size, train=False, sample_patches=None, angular_patch_size=5):
    # wrapper function to call all the above functions from the main code.
    img = load_image_from_db(db, index)
    patches = patch(img, patch_size, train, sample_patches, angular_patch_size=angular_patch_size)
    return img, patches


def load_image(image_file_path, patch_size, train=False, sample_patches=None, angular_patch_size=5):
    # wrapper function to call the above functions from the main code, the input image file is provided rather than from lmdb db
    img = np.expand_dims(cv2.imread(image_file_path, 0), axis=-1)
    print('@@@ img.shape', img.shape)
    patches = patch(img, patch_size, train, sample_patches, angular_patch_size=angular_patch_size)
    return patches


def reduce_all_patches(patches, reduction_int):
    # reduce the patches size - scale down the patches, this is iterative procedure, not used anywhere.
    image = np.empty(shape=(patches.shape[0], patches.shape[1]//reduction_int, patches.shape[1]//reduction_int, 3))
    index = 0
    for each_patch in patches:
        image[index] = reduce_resolution(each_patch, reduction_int)

    return image


def reduce_resolution(each_image, reduction_int):
    # reduce resoltuion of each image.
    new_image = cv2.resize(each_image, dsize=(each_image.shape[0]//reduction_int, each_image.shape[1]//reduction_int))
    return new_image


def patch_angular(lf_array, angular_patch_size=5):
    # do angular patches
    channels = lf_array.shape[-1]
    assert len(lf_array.shape) >= 5
    s = lf_array.shape[3]
    t = lf_array.shape[4]
    h = lf_array.shape[1]
    w = lf_array.shape[2]
    # get hte s, t, h, w array size

    # get the multiplier to be multiplied to angular_patch_size so that even patches could be created.
    s_multiplier = math.ceil(s/angular_patch_size)
    t_multiplier = math.ceil(t/angular_patch_size)

    all_patches = []
    for ii in range(lf_array.shape[0]):
        # for each s, and t we obtained new patches that are equal to math.ceil(s / angualr_patch_size) * math.ceil(t/angular_patch_size)

        num_patches = s_multiplier * t_multiplier

        new_lf_array = np.zeros(shape=(num_patches, h, w, angular_patch_size, angular_patch_size, channels))

        for i in range(s_multiplier):
            for j in range(t_multiplier):
                index = i*t_multiplier + j
                temp = lf_array[ii:ii+1, :, :, (i*angular_patch_size):(i+1)*angular_patch_size, j*angular_patch_size: (j+1)*angular_patch_size, ...]
                new_lf_array[index, :, :, :temp.shape[3], :temp.shape[4], :] = temp

        # append current patches obtained here.
        all_patches.append(new_lf_array)

    # concatenate all the patches.
    print('===>', np.concatenate(all_patches, axis=0).shape)
    return np.concatenate(all_patches, axis=0)


def increase_resolution(each_image, increase_int):
    # increate the resolutoin of the 4d image.
    new_image = cv2.resize(each_image, dsize=(each_image.shape[0]*increase_int, each_image.shapae[1]*increase_int))
    return new_image


def restore_patches(patches, img_shape):
    # retoring hte spatial patches.
    if len(img_shape) == 3:
        realH, realW, _ = img_shape
    if len(img_shape) == 5:
        H = img_shape[0]
        W = img_shape[1]
    elif len(img_shape) == 6:
        H = img_shape[1]
        W = img_shape[2]
    print(patches.shape)
    # _, H, W, _ = patches.shape
    H_prime = math.ceil(H1/H)*H
    W_prime = math.ceil(W1/W)*W
    H_multiplier = H_prime // H
    W_multiplier = W_prime // W
    patches_size = 196 # 14X14 - angular patches
    image = np.zeros(shape=(patches_size, H_prime, W_prime, 3))
    print(image.shape)
    i = 0
    index = 0
    while index < patches.shape[0]:
        # loop through all the patches and assign that to above image variable
        for j in range(H_multiplier):
            for k in range(W_multiplier):
                image[i, j*H:(j+1)*H, (k)*W:(k+1)*W] = patches[index]
                index = index + 1
        i += 1

    # reshape the image variable to the actual image size
    lf_image = image[:, :H1, :W1].reshape((14, 14, H1, W1, 3))
    # if need to get it in terms of raw image rather than lf image.
    if len(img_shape) == 3:
        # get a zeros array with raw image shape
        actual_image = np.zeros(shape=(realH + realH % H1, realW + realW % W1, 3))
        S = 14
        T = 14
        for x in range(S):
            for y in range(T):
                temp = lf_image[x, y, :H1, :W1, :]
                # for each Sth and Tth pixel starting from x, y position (angular position), assign the patches restored above to obtain the actual image
                actual_image[x::S, y::T, :][:temp.shape[0], :temp.shape[1], :] = temp

        return actual_image[:realH, :realW, :]
    else:
        return lf_image


def load_data_db(db_path):
    # load lmdb
    db = lmdb.open(db_path)
    # load hte csv file as well.
    file = pd.read_csv(os.path.dirname(db_path) +'/' + db_path.split('/')[-1].split('.')[0] + '.csv').values.tolist()
    return db, file


def load_image_from_db(db, index):
    # load hte image from the db given its index
    with db.begin() as txn:
        # get the image bytes
        image_bytes = txn.get('name_{}'.format(index).encode())
        # get hte actual image from the bytes and reshape to the lf array
        img = np.frombuffer(zlib.decompress(image_bytes), dtype=np.uint8).reshape((14, 14, H1, W1, 3))

    return img


def load_train_test_validation_files(data_file_path, load_db=False):
    # load all 3 dbs and files at once
    train = pd.read_csv(data_file_path + '/train.csv')
    test = pd.read_csv(data_file_path + '/test.csv')
    validation = pd.read_csv(data_file_path + '/validation.csv')

    if load_db:
        train_db = lmdb.open(data_file_path + '/train.db')
        test_db = lmdb.open(data_file_path + '/test.db')
        validation_db = lmdb.open(data_file_path + '/validation.db')

        return (train, train_db), (validation, validation_db), (test, test_db)

    return (train, None), (validation, None), (test, None)


def convert_rgb_to_gray(img):
    # convert hte rgb image to gray
    new_image = np.zeros(shape=img.shape[0:-1])
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            new_image[i, j] = cv2.cvtColor(img[i, j, ...].astype('uint8'), cv2.COLOR_RGB2GRAY)

    return np.expand_dims(new_image, axis=-1)
