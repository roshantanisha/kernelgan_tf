import tensorflow as tf
from networks_tf import *
from loss_tf import *
from easydict import EasyDict
from data_creation_on_fly import *
from data_tf import *
import lmdb
import zlib
from tqdm import tqdm


class KernelGAN:
    def __init__(self, conf, filename):
        """
        Args:
            conf: configuration object
            filename: directory name for storing the kernels
        """
        self.conf = conf
        # create generator object
        self.G = Generator(self.conf)
        print('@@@@@@@@ -> ', self.G.output_size)
        # create discriminator object
        self.D = Discrimator(conf, [1] + self.G.output_size[1:-1] + [3], self.G.forward_shave)
        # initialize models
        self.init_model()
        self.filename = filename
        if not os.path.exists(self.filename):
            os.makedirs(self.filename)
            # create directory form the filename
        # self.lmdb_cursor = lmdb.open(filename + '_kernel', map_size=2**20)

    def train(self, g_input, d_input):
        # train networks
        self.train_g(g_input, d_input)
        self.train_d(g_input, d_input)

    def init_model(self):
        # create session objects
        self.sess_g = tf.Session(graph=self.G.graph)
        self.sess_d = tf.Session(graph=self.D.graph)
        # initialize the variables in networks
        with self.G.graph.as_default():
            self.sess_g.run(tf.global_variables_initializer())
        with self.D.graph.as_default():
            self.sess_d.run(tf.global_variables_initializer())

    def save_model(self, step):
        # save mdels at step
        if not os.path.exists('train_weights'):
            os.makedirs('./train_weights')
        with self.G.graph.as_default():
            saver = tf.train.Saver()
            saver.save(self.sess_g, 'train_weights/generator', global_step=step)
        with self.D.graph.as_default():
            saver = tf.train.Saver()
            saver.save(
                self.sess_d,
                'train_weights/discriminator', global_step=step
            )

    def train_g(self, g_input, d_input):
        loss_g = 0
        for e in tqdm(range(1, self.conf.epochs + 1)):
            # train for self.conf.epochs times for current input
            with self.G.graph.as_default():
                # get generator predictions for the generator input
                g_pred = self.sess_g.run(
                    self.G.final_layer,
                    {
                        self.G.input: g_input
                    }
                )
                g_pred = np.swapaxes(g_pred, axis1=0, axis2=-1)
                # swap axes for the discirminator moel

            with self.D.graph.as_default():
                # get the discirminator outout for the generator input - fake variable
                d_pred_fake = self.sess_d.run(
                    self.D.final_layer,
                    {
                        self.D.input: g_pred
                    }
                )
            with self.G.graph.as_default():
                # get all the loss values of the generator model
                value = self.sess_g.run(
                    [self.G.grad_op, self.G.total_loss, self.G.loss_sparse, self.G.loss_centralized, self.G.loss_boundaries, self.G.loss_bicubic, self.G.loss_sum2one, self.G.criterion_loss],
                    {
                        self.G.input: g_input,
                        self.G.d_pred_fake_placeholder:
                            d_pred_fake
                    }
                )

                # add the total loss loss_g variable which is averaged at the end.
                loss_g += value[1]
        print('Generator Loss => ', loss_g / self.conf.epochs)

    def train_d(self, g_input, d_input):
        loss_d = 0
        for e in tqdm(range(1, self.conf.epochs + 1)):
            # train discriminator for self.conf.epochs time
            with self.G.graph.as_default():
                # get the fake output for the generator input
                g_output = self.sess_g.run(
                    self.G.final_layer,
                    {
                        self.G.input: g_input
                    }
                )

            with self.D.graph.as_default():
                g_output = np.swapaxes(g_output, 0, -1)
                temp_img = g_output + np.random.uniform(size=g_output.shape)
                # generate fake image
                print(temp_img.shape, '%%%%%%%%%%%%%%%%%')

                # obtained the fake generative loss
                loss1 = self.sess_d.run(
                    [self.D.gan_loss1],
                    {
                        self.D.input: temp_img,
                        self.D.real_placeholder: False
                    }
                )

                # get hte total generative loss for the discirminator network
                loss2 = self.sess_d.run(
                    [self.D.train_op, self.D.final_loss],
                    {
                        self.D.input: np.swapaxes(d_input, 0, -1),
                        self.D.real_placeholder: True,
                        self.D.loss_placeholder: loss1[0]
                    }
                )

                # add the total loss to the loss_d which is averaged at hte end.
                loss_d += loss2[1]
        print(loss_d, self.conf.epochs)
        print('Discriminator Loss => ', loss_d / self.conf.epochs)

    def finalize(self, g_input, index):
        # save the kernels
        with self.G.graph.as_default():
            # get hte kernel imitating from the generator
            curr_k = self.sess_g.run(self.G.curr_k, {
                self.G.input: g_input
            })
        print(curr_k.shape, curr_k.dtype, 'curr_k kernel')
        # post process the kernel obtained above
        final_kernel = post_process_k(curr_k, self.conf.n_filtering)
        print(final_kernel.shape, final_kernel.dtype, 'final kernel')

        k_4 = None
        if self.conf.x4:
            # if interpolation to scale 4 then obtain that kernel as well.
            k_4 = analytic_kernel(final_kernel)
            # save scaled kernel
            np.save(self.filename + '/k_4kernel' + str(index), zlib.compress(k_4))

        # save the kernel
        save_final_kernel(final_kernel, self.conf, k_4)
        # with self.lmdb_cursor.begin(write=True) as txn:
        #     txn.put(str(index).encode(), zlib.compress(final_kernel))
        #     if self.conf.x4:
        #         txn.put((str(index) + '_k_4').encode(), zlib.compress(k_4))
        np.save(self.filename+'/kernel'+str(index), zlib.compress(final_kernel))
        # save the actual imitating kernel too
        np.save(self.filename + '/curr_kernel' + str(index), zlib.compress(curr_k))
        run_zssr(final_kernel, self.conf)


if __name__ == '__main__':
    conf = EasyDict()
    conf.G_chan = 64 # number of kenrels in first convolution channels
    conf.D_chan = 64 # number of kernels in first convolution channel
    conf.D_kernel_size = 7 # the angular and sptial kernel size for discriminator
    conf.D_n_layers = 7 # total number of layers in discriminator
    conf.G_structure = [7, 5, 3, 1, 1, 1] # the kernel size in the recursive layers in hgenerator
    conf.scale_factor = 0.5 # the downsampling scale factor
    conf.g_lr = 2e-4 # generator learning rate
    conf.d_lr = 2e-4 # discrimintor learning rate
    conf.beta1 = 0.5 # adam otpimizer beta1 value
    conf.beta2 = 0.999 # adam optimizer beta2 value
    conf.G_kernel_size1 = 6 # the generator angular imitating kernel size
    conf.G_kernel_size2 = 64 # the generator spatial imitating kernel size
    conf.x_dim = 4 # image patch angular dimension 1
    conf.y_dim = 4 # image patch angular dimension 2
    conf.z_dim = 32 # image patch spatial dimension 1
    conf.t_dim = 32 # image patch spatial dimension 2
    conf.input_crop_size = 32 # crop size of the spatial dimension in the 4d image
    conf.in_channels = 1 # number of input channel to generator
    conf.out_channels = 1 # output channle in both generator nad discirminator
    conf.n_filtering = 40 # post processing kernel to use nth largest value to centralize the kernel
    conf.output_dir_path = './' # outpout directory path where outputs kernel will be stored
    conf.img_name = 'test_img' # test image name used with ZSSR
    conf.x4 = False # whether to scale for interpolated images as well.
    conf.do_ZSSR = False # whether to perform ZSSR on postprocessing of kernel
    conf.X4 = False # whether to perform ZSSR on interpolated version as well
    conf.input_image_path = '/Users/tanishabhayani/Desktop/pisa.png' # image used with ZSSR, give the raw image path downloaded
    conf.real_image = '/Users/tanishabhayani/Desktop/pisa.png' # same as above
    conf.noise_scale = 1 # the noise added in ZSSR
    conf.epochs = 2 # number of epochs to train kernels
    conf.batch_size = 3 # this is the swapped channels to batches specifically for the generator, given to construct the shape while defining the network
    conf.data_file_path = '../../../../../data_processings/' # give path to data processings file
    conf.max_iters = 1 # how many times to sample to get hte dataset patch
    conf.output_size = int(conf.input_crop_size * conf.scale_factor) # output size frmo the generator is calculated here
    conf.forward_shave = 0 # forward shave is initialized with 0

    # input image format for both generator and discriminator is: x, y, z, t, c

    batch = 1
    x_dim = 14
    y_dim = 14
    z_dim = 376
    t_dim = 541
    in_channels = 3
    out_channels = 3
    scale_factor=0.5

    # laod train, validation and test databases.
    (train, train_db), (validation, validation_db), (test, test_db) = load_train_test_validation_files(conf.data_file_path, load_db=True)

    # training
    for each_index in range(10):
        # load model for each image
        model = KernelGAN(conf, 'train')
        # load the image from db for each_index
        img = load_image_from_db(train_db, each_index)
        # get the central image
        sub_img = img[7, 7, ...]
        # generate the dataset
        dt = DataGenerator(conf, sub_img)

        print(len(dt))

        index = 1
        for data in dt:
            # get the top, left indices to draw the patch from the image at each_index
            _, top, left, size = data
            print('===>', top, left, size)
            # get the angular indices, equal to x_dim (angualr patch size)
            indices0 = np.random.choice(np.arange(14), conf.x_dim, replace=False)
            # create the generator data image
            g_data = np.swapaxes(img[np.newaxis, indices0][:,:, indices0, top[0] : top[0] + size[0], left[0]: left[0] + size[0], :], 0, -1)
            print(g_data.shape, '$$$$%%%%%')
            # generate hte angualr patch dimensions for which is scaled down to scale factor of x_dim
            indices1 = np.random.choice(np.arange(14), size=int(conf.x_dim*conf.scale_factor), replace=False)
            print(indices1)
            # create the discriminatro datapoint
            d_data = np.swapaxes(np.expand_dims(img[indices1, :, :, :][:, indices1, top[1]: top[1] + size[1], left[1]: left[1] + size[1], :], axis=0), 0, -1)
            print(d_data.shape, '&&&&&&&&**')

        # _, gen_patches = load_patches(train_db, each_index, patch_size=conf.z_dim, train=True, angular_patch_size=conf.x_dim, sample_patches=1)
        #
        # _, dis_patches = load_patches(train_db, each_index, patch_size=int(conf.z_dim*conf.scale_factor), train=True,
        #                               angular_patch_size=int(conf.x_dim*conf.scale_factor), sample_patches=1)
        #
        # print(gen_patches[1].shape, dis_patches[1].shape)

        # g_data = np.swapaxes(np.swapaxes(np.swapaxes(gen_patches[1], 1, 3), 2, 4), 0, -1)
        # d_data = np.swapaxes(np.swapaxes(np.swapaxes(dis_patches[1], 1, 3), 2, 4), 0, -1)

            if index % 100 == 0:
                # save model at 100th index
                model.save_model(index)
            # normalize data points and pass to train the models
            model.train(g_data / 255., d_data / 255.)
            # save the kernels at evert step.
            model.finalize(g_data, each_index)

            # model.lmdb_cursor.close()
            del model

            index += 1

        import sys
        sys.exit(-1)

    # same procedure is repeated for testing dataset
    for each_index in range(2):
        model = KernelGAN(conf, 'test')
        img = load_image_from_db(test_db, each_index)
        sub_img = img[7, 7, ...]
        dt = DataGenerator(conf, sub_img)

        print(len(dt))

        for data in dt:
            _, top, left, size = data
            indices0 = np.random.choice(np.arange(14), conf.x_dim, replace=False)
            g_data = np.swapaxes(
                img[np.newaxis, indices0][:, :, indices0, top[0]: top[0] + size[0], left[0]: left[0] + size[0], :], 0,
                -1)
            print(g_data.shape, '$$$$%%%%%')
            indices1 = np.random.choice(np.arange(14), size=int(conf.x_dim * conf.scale_factor), replace=False)
            print(indices1)
            d_data = np.swapaxes(np.expand_dims(
                img[indices1, :, :, :][:, indices1, top[1]: top[1] + size[1], left[1]: left[1] + size[1], :], axis=0),
                                 0, -1)
            print(d_data.shape, '&&&&&&&&**')

            model.train(g_data, d_data)
            model.finalize(g_data, each_index)

            # model.lmdb_cursor.close()
            del model


    train_db.close()
    test_db.close()
    validation_db.close()
