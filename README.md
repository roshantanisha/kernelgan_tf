# Objective (Part 2):
Implement a degradation model for real-world images by estimating various blur kernels as well as real noise distributions: Most current approaches rely on paired low and high-resolution images to train the network in a fully supervised manner. However, such image pairs are not available in real-world applications. Thus, the aim here is instead to learn super-resolution from unpaired data and without any restricting assumptions on the input image formation. 


# Introduction:

"KernelGAN" – an image-specific Internal-GAN, which estimates the SR kernel that best preserves the distribution of patches across scales of the LR image. Its Generator is trained to produce a downscaled version of the LR test image, such that its Discriminator cannot distinguish between the patch distribution of the downscaled image, and the patch distribution of the original LR image. In other words, G trains to fool D to believe that all the patches of the downscaled image were actually taken from the original one. The Generator, once trained, constitutes the downscaling operation with the correct image-specific SR-kernel. KernelGAN is fully unsupervised, requires no training data other than the input image itself. Since downscaling by the SR-kernel is a linear operation applied to the LR image (convolution and subsampling), Generator (as opposed to the Discriminator) is a linear network (without non-linear activations). At first glance, it may seem that a single-strided convolution layer should suffice as a Generator. Interestingly, using a deep linear network is dramatically superior to a single-strided one. KernelGAN is an image specific GAN that trains on a single input image. It consists of a downscaling generator (G) and a discriminator (D). Both G and D are fully-convolutional, which implies the network is applied to patches rather than the whole image. Given an input image I<sub>LR</sub>, G learns to downscale it, such that, for D, at the patch level, it is indistinguishable from the input image I<sub>LR</sub>. D trains to output a heat map, referred to as D-map indicating for each pixel, how likely is its surrounding patch to be drawn from the original patch-distribution. It alternately trains on real examples (crops from the input image) and fake ones (crops from G’s output). The loss is the pixel-wise MSE difference between the output D-map and the label map. The labels for training D are a map of all ones for crops extracted from the original LR image, and a map of all zeros for crops extracted from the downscaled image. SR kernels are not only image specific, but also depend on the desired scale-factor s. However, there is a simple relation between SR-kernels of different scales. We are thus able to obtain a kernel for SR ×4 from G that was trained to downscale by ×2. This is advantageous for two reasons: First, it allows extracting kernels for various scales by one run of KernelGAN. Second, it prevents downscaling the LR image too much. Small LR images downscaled by a scale factor of 4 may result in tiny images (×16 smaller than the HR image) which may not contain enough data to train on. KernelGAN is trained to estimate a kernel k2 for a scale-factor of 2.


# Full Network Architecture:
**1. Input to the Network**: Actual Images

**2. Output of the Network**: Downsampled images (degraded images). The purpose is to learn the kernels that downgrade the images.

**3. How many layers?**: 
 * Generator:  3 Layers with 2 convolution layer and 5 feature blocks with different kernel size
 * Discriminator: 3 Layers with 2 convolution layer and 7 feature block layers

**4. What does each layer consist of (conv, transposed conv, ReLU, Batch Normalization, weight layer, etc.)? and how many of each?**
 * Generator Feature Block: Convolution Layer with different kernel sizes
 * Discriminator Feature Block: Convolution -> Spectral Norm -> Batch Norm -> ReLU

**5. How are these layers connected? (You can show them by drawing/figure if it’s hard to explain).**

<h2><p align="center">
Discriminator</p></h2>

![Discriminator Graph](discriminator_complete.png)

<h2><p align="center">
Generator</p></h2>

![Generator Graph](generator_complete.png)


**6. How may filters?** 
  * Generator: 7
  * Discriminator: 7

**7. Filter sizes?**

[(kernel_size, kernel_size)Angular (kernel_size, kernel_size)Spatial out_channels] X number_of_times_repeated
  * Generator:
    * [(7,7)A (7,7)S 64] X 1
    * [(5,5)A (5,5)S 64] X 1
    * [(3,3)A (3,3)S 64] X 1
    * [(1,1)A (1,1)S 64] X 1
    * [(1,1)A (1,1)S 64] X 1
    * [(1,1)A (1,1)S 64] X 1
    * [(3,3)A (3,3)S 1] X 1
  * Discriminator:
    * [(7,7)A (7,7)S 64] X 1
    * [(1,1)A (1,1)S 64] X 5
    * [(1,1)A (1,1)S 1] X 1

**8. What is the batch size?** 1

**9. Does it have a patch discriminator?** Yes

**10. What loss functions? How is it calculated?**
  * Generator:
    * Bicubic Loss - Mean squared error between the generated image and the application of bicubic kernel on the generated image
    * sum2one Loss - Mean Absolute Difference between ones shaped like the final kernel weights and final kernel weights  
    * boundaries loss - Mean Absolute Difference between the multiplication of scaled gaussian kernel and kernel weights and zero labels of shape like kernel weights
    * centralized loss - Mean Squared Error between weighted center of mass of the kernel and actual center of mass.
    * sparse loss - Mean Absolute Error between the contrasted kernel and zeros array shaped like kernel 
    * total loss - weighted sum of above losses
  * Discriminator: 
    * Generator Loss - Mean Absolute Error between the actual labels, and predicted ones.


# How is the training performed?

**1. Did you crop the images?** Yes, cropped to patches to (6, 6, 64, 64)

**2. Any data augmentation?** No

**3. Number of epochs?** 1

**4. Training parameters: momentum parameter? Learning rate initialization? Decay after how many epochs? Decay factor?** No decaying is performed. Learning rate for Generator and Discriminator both is 2e-4

**5. Loss function over all? Or different for each update?** We check total gan loss and discriminator loss and not individual losses.

**6. Evaluation metrics?** Total Gan Loss and Discriminator loss.


# Third-party code:

**1. What did you keep the same? Or how is it similar to each code?** We converted the 2D Kernel estimation torch to 4D images Kernel Estimation in tensorflow, the code is exactly the same just that the 2D KernelGAN

**2. What modifications did you do in details please?** `imresize.py` is modified completely to fit for 4D images. Convolution4D code which was available in tensorflow was added to accomodate the activation functions and graph details as well, along with the name of the layers too.   

**3. Why did you do these modifications? Like what is the purpose of the modifications?** To make it work for 4D images.



# System Specification
* OS: MacOS Catalina version: 10.15.6
* Python Version: Python3.7 (miniconda environment)
* Deep Learning Framework: Tensorflow version 1.14
* Dependencies
  * `tensorflow==1.15`
  * `tqdm==4.62.0`
  * `opencv-python==4.5.3.56`
  * `unrar==0.4` 
  * `scipy==1.1.0`
  * `matplotlib==3.4.3` 
  * `lmdb==1.2.1` 
  * `pandas==1.3.2` 
  * `sklearn==0.0` 
  * `easydict==1.9` 
  * `torch==1.9.0` 
  * `tensorboardX==2.4`
* Deep Learning network: N/A
* Third Party Code: 
    * https://github.com/jixiaozhong/RealSR
    * https://github.com/Tencent/Real-SR/
* Third Party Compute: N/A
* To train the KernelGAN to product the kernels run `KernelGAN_tf.py` as `python KernelGAN_tf.py` after activating the `tf` miniconda environment. 

# Steps to run the code and the setup
* Fist goto: https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html to install miniconda
* Then run `init_env.sh` to initialize the environment
* Go to `data_download` folder and run `download_datasets.sh` to download the dataset and uncompress it
* ~~Go to `vgg19` folder and run `download_pretrained_vgg19.sh` to download the pretrained weights~~
* Go to the folder `data_processings` and run `stanford.py` to create `lmdb` database of the images -> `Train`, `Test`, and `Validation` and corresponding `csv` files which contains the images path for each Train, Test and Validation images respectively.
* Now the setup is complete
* Now open the terminal, and type: `conda activate tf` - this will activate the python binary and respective packages installed in `tf` environment
* To train the KernelGAN to product the kernels run `KernelGAN_tf.py` as `python KernelGAN_tf.py` after activating the `tf` miniconda environment. 

# More information on the code.
* Here there is no train and test as separate dataset, as we are basically learning the kernel for dataset, so though the code includes 2 separate loops for train and test, as the data processed is train, test, etc.
