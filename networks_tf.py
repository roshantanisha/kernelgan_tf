import tensorflow as tf
from utils import *
# from tensorflow_addons.layers import SpectralNormalization
from easydict import EasyDict
from log import logging
from loss_tf import *
from convolution4d import *


class Generator:
    # Constraint co-efficients
    lambda_sum2one = 0.5
    lambda_bicubic = 5
    lambda_boundaries = 0.5
    lambda_centralized = 0
    lambda_sparse = 0
    lambda_criterion = 0.1

    def __init__(self, conf):
        """
        Args:
            conf: the configuration object
        """
        self.graph = tf.Graph()
        self.conf = conf
        self.model = self.build_generator()
        self.create_loss()

    def build_generator(self):
        with self.graph.as_default():
            self.input = tf.placeholder(shape=(self.conf.batch_size, self.conf.x_dim, self.conf.y_dim, self.conf.z_dim, self.conf.t_dim, self.conf.in_channels), dtype=tf.float32)
            struct = self.conf.G_structure

            with tf.name_scope('conv1'):
                # First layer - Converting 4d image to latent space
                self.first_layer = conv4d(self.input, in_channels=self.conf.in_channels, out_channels=self.conf.G_chan, kernel_size_1=struct[0], kernel_size_2=struct[0], bias=False, stddev_factor=0.1, name='first' + '_generator', padding='SAME', graph=self.graph,activation='linear')

            self.feature_block = [] # recursive feature blocks

            temp = self.first_layer

            for layer in range(1, len(struct) - 1): # Stacking intermediate layer according to the conf.G_structure
                with tf.name_scope('feature_block' + str(layer)):
                    self.feature_block.append(
                        conv4d(temp, in_channels=self.conf.G_chan, out_channels=self.conf.G_chan, kernel_size_1=struct[layer], kernel_size_2=struct[layer], bias=False, stddev_factor=0.1, name=str(layer) + '_generator', padding='SAME', graph=self.graph,activation='linear')
                    )
                    temp = self.feature_block[-1]

            with tf.name_scope('final_layer'):
                # Final layer - Down-sampling and converting back to image
                self.final_layer = conv4d(
                    temp, in_channels=self.conf.G_chan, out_channels=self.conf.out_channels,
                    stride_1=int(1/self.conf.scale_factor), stride_2=int(1/self.conf.scale_factor), bias=False,
                    stddev_factor=0.1, name='final_generator', padding='SAME', graph=self.graph,activation='linear'
                )

            # Calculate number of pixels shaved in the forward pass
            self.output_size = self.final_layer.shape.as_list()
            print(self.output_size)
            self.forward_shave = int(self.conf.input_crop_size * self.conf.scale_factor) - self.output_size[2]

            return self.final_layer, self.output_size, self.forward_shave

    def create_loss(self):
        # calculate the combined loss for hte generator
        with self.graph.as_default():
            self.bicubic_kernel = get_bicubic_kernel(self.graph, self.conf.in_channels)
            self.bicubic_loss = DownsaleLoss
            self.sum2one_loss = SumOfWeightsLoss
            self.boundaries_loss = BoundariesLoss
            self.centralized_loss = CentralizedLoss
            self.sparse_loss = SparsityLoss

            self.loss_bicubic = 0

            # define the adam optimizer
            self.g_opt = tf.train.AdamOptimizer(learning_rate=self.conf.g_lr, beta1=self.conf.beta1, beta2=0.999)

            # define the placholder for the disciminator output to be used in centralized loss
            self.d_pred_fake_placeholder = tf.placeholder(shape=[1] + self.output_size[1:-1] + [1], dtype=self.input.dtype)
            print('######### -> ', self.d_pred_fake_placeholder)

            with tf.name_scope('Centralized_Loss'):
                self.criterion_loss = self.centralized_loss(self.d_pred_fake_placeholder)

            self.total_loss = self.lambda_criterion * self.criterion_loss + self.calc_constraints()

            gv = self.g_opt.compute_gradients(self.total_loss)
            self.grad_op = self.g_opt.apply_gradients(gv)

            print('grad_op = ', self.grad_op)

    def calc_constraints(self):
        # Calculate Kernel which is equivalent to G
        with self.graph.as_default():
            with tf.name_scope('curr_k'):
                self.curr_k = calc_curr_k(self, graph=self.graph)
            # Calculate constraints, for more information on inpuit and output refer each function separately
            with tf.name_scope('loss_bicubic'):
                self.loss_bicubic = self.bicubic_loss(self.input, self.final_layer, self.conf.scale_factor, self.bicubic_kernel, graph=self.graph)
            with tf.name_scope('loss_boundaries'):
                self.loss_boundaries = self.boundaries_loss(self.curr_k)
            with tf.name_scope('loss_sum2one'):
                self.loss_sum2one = self.sum2one_loss(self.curr_k, self.graph)
            with tf.name_scope('loss_centralized'):
                self.loss_centralized = self.centralized_loss(self.curr_k)
            with tf.name_scope('loss_sparse'):
                self.loss_sparse = self.sparse_loss(self.curr_k)

            # calculate weighted loss here.
            return self.loss_bicubic * self.lambda_bicubic + self.loss_sum2one * self.lambda_sum2one + self.loss_boundaries * self.lambda_boundaries + self.loss_centralized * self.lambda_centralized + self.loss_sparse * self.lambda_sparse


class Discrimator:
    def __init__(self, conf, output_size, forward_shave):
        """
        Args:
            conf: the configuration object
            output_size: the output size from the generator that has to be the input size for the disciriminator
            forward_shave: whether any shaving of pixel is required is calculated at the end of hte model building.
        """
        self.graph = tf.Graph()
        self.conf = conf
        self.output_size = output_size
        self.forward_shave = forward_shave
        self.model = self.build_discriminator()
        self.create_loss()

    def build_discriminator(self):
        with self.graph.as_default():
            self.input = tf.placeholder(shape=self.output_size, dtype=tf.float32)
            with tf.name_scope('conv1'):
                # First layer - Convolution (with no ReLU)
                temp = conv4d(self.input, in_channels=3, out_channels=self.conf.D_chan, kernel_size_2=self.conf.D_kernel_size, kernel_size_1=self.conf.D_kernel_size, bias=True, stddev_factor=0.1, name='0' + '_discriminator', graph=self.graph, activation='linear')
                self.first_layer = spectral_norm(temp, name='_discriminator', graph=self.graph)
                # self.first_layer = temp

            self.feature_block = []
            temp = self.first_layer
            for l in range(1, self.conf.D_n_layers - 1): # Stacking layers with 1x1 kernels
                with tf.name_scope('feature_block_disc' + str(l)):
                    l1 = conv4d(temp, in_channels=self.conf.D_chan, out_channels=self.conf.D_chan, kernel_size_1=1, kernel_size_2=1,
                               bias=True, stddev_factor=0.1, name=str(l) + '_discriminator', graph=self.graph)
                    snc = spectral_norm(
                        l1, '_discriminator' + str(l), graph=self.graph
                    )
                # snc = l1
                    with self.graph.as_default():
                        bn = tf.nn.batch_normalization(snc, mean=1.0, variance=0.02, offset=0, scale=1, variance_epsilon=1e-8, name=str(l) + 'batch_discriminator')
                        relu = tf.nn.relu(bn, name=str(l) + '_relu_discriminator')
                    self.feature_block.append(
                        relu
                    )
                    temp = self.feature_block[-1]

            with tf.name_scope('final_conv'):
                self.final_layer = conv4d(temp, in_channels=self.conf.D_chan, out_channels=self.conf.out_channels, kernel_size_1=1, kernel_size_2=1, bias=True, activation='sigmoid', stddev_factor=0.1, name='final_discriminator', graph=self.graph)
                print('final layer = ', self.final_layer)

            # Calculate number of pixels shaved in the forward pass
            self.forward_shave = self.conf.input_crop_size - self.final_layer.shape.as_list()[2]

        return self.final_layer

    def create_loss(self):
        # calculate the discirminator loss which is 0.5*(real_gan_loss + fake_gan_loss)
        with self.graph.as_default():
            self.real_placeholder = tf.placeholder(shape=(), dtype=tf.bool)
            # calculate the real_gan_loss first
            self.gan_loss1 = GANLoss(self.final_layer, self.real_placeholder, graph=self.graph)
            self.loss_placeholder = tf.placeholder(shape=self.gan_loss1.shape, dtype=self.gan_loss1.dtype)
            # use the above real_gan_loss to calculate the final loss here.
            self.gan_loss2 = GANLoss_disc(self.final_layer, self.gan_loss1, self.real_placeholder, graph=self.graph)
            self.final_loss = self.gan_loss2
            # define the optimizer
            self.opt = tf.train.AdamOptimizer(learning_rate=self.conf.d_lr, beta1=0.999, beta2=self.conf.beta2)
            # compute the gradients from the loss
            self.gv = self.opt.compute_gradients(self.final_loss)
            # apply the gradients to the network.
            self.train_op = self.opt.apply_gradients(self.gv)


if __name__ == '__main__':
    conf = EasyDict()
    conf.G_chan = 64
    conf.D_chan = 64
    conf.D_kernel_size = 7
    conf.D_n_layers = 7
    conf.G_structure = [7, 5, 3, 1, 1, 1]
    conf.scale_factor = 0.5
    conf.g_lr = 2e-4
    conf.d_lr = 2e-4
    conf.beta1 = 0.5
    conf.beta2 = 0.999
    conf.G_kernel_size1 = 6
    conf.G_kernel_size2 = 64
    conf.x_dim = 6
    conf.y_dim = 6
    conf.z_dim = 64
    conf.t_dim = 64
    conf.input_crop_size = 64
    conf.in_channels = 1
    conf.out_channels = 1
    conf.n_filtering = 40
    conf.output_dir_path = './'
    conf.img_name = 'test_img'
    conf.x4 = False
    conf.do_ZSSR = False
    conf.X4 = False
    conf.input_image_path = '/Users/tanishabhayani/Desktop/pisa.png'
    conf.real_image = '/Users/tanishabhayani/Desktop/pisa.png'
    conf.noise_scale = 1
    conf.epochs = 1
    conf.batch_size = 3
    conf.data_file_path = '../../../../../data_processings/'  # give path to data processings file
    conf.max_iters = 1
    conf.output_size = int(conf.input_crop_size * conf.scale_factor)
    conf.forward_shave = 0


    #
    # gen = Generator()
    # gen_model = gen.build_generator(conf, a)
    # #
    # print(gen_model, a)
    #
    # dis = Discrimator(conf)
    # with dis.graph.as_default():
    #     sess = tf.Session(graph=dis.graph)
    #     sess.run(tf.global_variables_initializer())
    #     value = sess.run(
    #         dis.train_op,
    #         {
    #             dis.real_placeholder: True,
    #             dis.input: np.zeros(shape=dis.input.shape)
    #         }
    #     )
    #     print('value = ', value)
    #     value = sess.run(
    #         [dis.final_loss, dis.final_layer],
    #         {
    #             dis.real_placeholder: True,
    #             dis.input: np.zeros(shape=dis.input.shape)
    #         }
    #     )
    #     print(value[0], value[1].shape)
    #     print(dis.final_layer.dtype, dis.final_layer.shape)

    # gen = Generator(conf)
    # print('gen.curr_k = ', gen.curr_k.shape)
    # with gen.graph.as_default():
    #     sessg = tf.Session(graph=gen.graph)
    #
    #     sessg.run(tf.global_variables_initializer())
    #
    #     with gen.graph.as_default():
    #         saver = tf.train.Saver()
    #         saver.save(sessg, 'train_weights/generator', global_step=0)

    # o = tf.nn.conv3d(tf.zeros(shape=(2,2,5,5,3)), tf.zeros(shape=(1,1,1,1,3)), strides=[1,1,1,1,1], padding='SAME', data_format='NDHWC')
    #
    # print(o.shape)
    #
    # print(dis_model)

    # g = dis.graph
    # with g.as_default():
    #     opt = tf.train.AdamOptimizer()
    #     loss = dis.gan_loss()
    #     opt_op = opt.minimize(loss)
    # writer = tf.summary.FileWriter(logdir='logdir', graph=g)
    # writer.flush()
    # with g.as_default():
    #     sess = tf.Session(graph=g)
    #     sess.run(tf.global_variables_initializer())
    #     value, l = sess.run([opt_op, loss], {
    #             dis.input: np.zeros(
    #                 shape=(dis.input.shape)
    #             ),
    #         y_zeros: np.zeros(shape=(dis.final_layer.shape)),
    #         y_ones: np.ones(shape=(dis.final_layer.shape))
    #         }
    #     )
    #     print(value, l)
    #     print(opt_op)

    gen = Generator(conf)
    with gen.graph.as_default():
        a = tf.zeros(
            shape=[batch, x_dim, y_dim, z_dim, t_dim, in_channels]
        )
        model = gen.build_generator()
        loss = 1.0
        sess = tf.Session(graph=gen.graph)
        sess.run(tf.global_variables_initializer())
        opt = tf.train.AdamOptimizer()
        for w in tf.trainable_variables():
            print(w.name)
            gv = opt.compute_gradients(tf.constant(loss), w)
            opt.apply_gradients(gv)

